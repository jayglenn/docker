version: '3.6'

secrets:
    vpn-user:
        file: ./.secrets/vpn-user.txt
    vpn-password:
        file: ./.secrets/vpn-password.txt 
    code-url:
        file: ./.secrets/code-url.txt
    code-password:
        file: ./.secrets/code-password.txt
    code-sudo-password:
        file: ./.secrets/code-sudo-password.txt
    bitwarden-url:
        file: ./.secrets/bitwarden-url.txt
    wireguard-url:
        file: ./.secrets/wireguard-url.txt
    mssql-password:
        file: ./.secrets/mssql-password.txt
    pihole-password:
        file: ./.secrets/pihole-password.txt
    
services:

    swag:
        image: lscr.io/linuxserver/swag
        container_name: swag
        cap_add:
        - NET_ADMIN
        environment:
        - PUID=1000
        - PGID=1000
        - TZ=America/New_York
        - URL=internet-site.com
        - VALIDATION=dns
        - SUBDOMAINS=vault,
        - DNSPLUGIN=cloudflare
        - ONLY_SUBDOMAINS=true #optional
#       - MAXMINDDB_LICENSE_KEY= #optional
        volumes:
        - ./containers/swag:/config
        ports:
        - 443:443
        - 80:80
        restart: unless-stopped

    tautulli:
        container_name: tautulli
        restart: always
        image: linuxserver/tautulli:latest
        volumes:
        - ./tautulli:/config
        environment:
        - TZ=America/New_York
        ports:
        - 8181:8181/tcp
    
    vpn:
        container_name: vpn
        image: bubuntux/nordvpn:latest
        network_mode: bridge                    # Required
        restart: on-failure:2
        cap_add:
        - NET_ADMIN                             # Required
        - SYS_MODULE                            # Required for TECHNOLOGY=NordLynx
        sysctls:
        - net.ipv4.conf.all.rp_filter=2
        devices:
        - /dev/net/tun
        secrets:
#        - vpn-user
        - vpn-password
        ports:
        - 8282:8080                             # sabnzbd
        - 8989:8989/tcp                         # sonarr
        - 7878:7878/tcp                         # radarr
        - 3128:3128                             # proxy
        environment:
        - USER=[REDACTED]
        - CONNECT=Canada
        - TECHNOLOGY=NordLynx
        - NETWORK=192.168.0.0/16
        - CYBER_SEC=Enable
        - TZ=America/New_York
        - PASSFILE=/run/secrets/vpn-password
#        ulimits:                                # Recommended for High bandwidth scenarios
#            memlock:
#                soft: -1
#                hard: -1
                                      

    bitwarden:
        container_name: bitwarden
        restart: always
        image: vaultwarden/server:latest
        volumes:
        - ./containers/bitwarden:/data
        environment:
        - SIGNUPS_ALLOWED=false
        - INVITATIONS_ALLOWED=false
        - WEBSOCKET_ENABLED=true


    code-server:
        container_name: code-server
        restart: always
        image: linuxserver/code-server:latest
        ports: 
        - 9443:8443/tcp
        secrets:
        - code-url
        - code-password
        - code-sudo-password
        environment:
        - PUID=1000
        - PGID=1000
        - DOCKER_MODS=linuxserver/mods:code-server-python3
        - TZ=America/New_York
        - PROXY_DOMAIN_FILE=/run/secrets/code-url
        - PASSWORD_FILE=/run/secrets/code-password
        volumes:
        - ./code-server/config:/config
        - ./code-server/projects:/projects

    nextcloud:
        container_name: nextcloud
        restart: always
        image: ghcr.io/linuxserver/nextcloud
        ports:
        - 433:443/tcp
        volumes:
        - ./nextcloud/data:/data
        - ./nextcloud/config:/config
        environment:
        - TZ=America/New_York
        - PUID=1000
        - PGID=1000

    sabnzbd:
        container_name: sabnzbd
        restart: always
        image: linuxserver/sabnzbd:latest
        environment:
        - TZ=America/New_York
        - PUID=1000
        - PGID=1000
        volumes:
        - ./sabnzbd/config:/config
        - ./sabnzbd/downloads:/downloads
        network_mode: service:vpn
        depends_on:
        - vpn


    unifi:
        container_name: unifi
        restart: always
        image: linuxserver/unifi-controller:latest
        ports:
        - 10001:10001/udp
        - 3478:3478/udp
        - 6789:6789/tcp
        - 8080:8080/tcp
        - 8081:8081/tcp
        - 8443:8443/tcp
        - 8843:8843/tcp
        - 8880:8880/tcp
        environment:
        - MEM_LIMIT = 1024
        volumes:
        - ./unifi-controller:/config

    radarr:
        container_name: radarr
        restart: always
        image: linuxserver/radarr:latest
        environment:
        - TZ=America/New_York
        - PUID=1000
        - PGID=1000
        volumes:
        - /media/plex/movies:/media/plex/movies
        - ./radarr:/config
        - ./sabnzbd/downloads/complete/movies:/downloads
        network_mode: service:vpn
        depends_on:
        - vpn

    plex:
        container_name: plex
        restart: always
        image: ghcr.io/linuxserver/plex
        ports: 
        - 32400:32400/tcp
        environment:
        - TZ=America/New_York
        - PUID=1000
        - PGID=1000
        volumes:
        - /media/plex/tv:/tv
        - ./plex/config:/config
        - /dev/shm:/transcode
        - ./plex/movies:/movies
        
    watchtower:
        container_name: watchtower
        restart: always
        image: containrrr/watchtower:latest
        volumes:
        - /var/run/docker.sock:/var/run/docker.sock
        command: "--interval 604800 --cleanup"  # run once every 7 days

    portainer:
        container_name: portainer
        restart: always
        image: portainer/portainer-ce:latest
        ports:
        - 9000:9000/tcp
        volumes:
        - ./portainer:/data
        - /var/run/docker.sock:/var/run/docker.sock

    lazylibrarian:
        container_name: lazylibrarian
        restart: always
        image: ghcr.io/linuxserver/lazylibrarian
        ports: 
        - 5299:5299/tcp
        environment:
        - TZ=America/New_York
        volumes:
        - ./lazylibrarian:/config
        - /media/library:/media
        - ./sabnzbd/downloads/complete/lazy:/downloads
        - /media/library/books:/books
    
    wireguard:
        container_name: wireguard
        cap_add:
        - NET_ADMIN
        - SYS_MODULE
        restart: always
        image: ghcr.io/linuxserver/wireguard
        ports:
        - 51820:51820/udp
        environment:
        - TZ=America/New_York
        - SERVERURL_FILE=/run/secrets/wireguard-url
        - SERVERPORT=51820
        - ALLOWEDIPS=0.0.0.0/0
        - INTERNAL_SUBNET=10.13.13.0
        - PEERDNS=auto
        - PEERS=XPS13
        - PUID=1000
        - PGID=1000
        secrets:
        - wireguard-url
        volumes:
        - /lib/modules:/lib/modules
        - ./wireguard/config:/config
    
    mssql:
        container_name: mssql
        restart: always
        image: mcr.microsoft.com/mssql/server:2019-latest
        secrets:
        - mssql-password
        environment:
        - ACCEPT_EULA=Y
        - MSSQL_PID=Standard
        volumes:
        - ./mssql:/var/opt/mssql
        ports:
        - 13001:1433/tcp
        
    sonarr:
        container_name: sonarr
        restart: always
        image: ghcr.io/linuxserver/sonarr
        volumes:
        - ./sonarr/config:/config
        - /media/plex/tv/:/media/plex/tv
        - ./sabnzbd/downloads/complete/tv:/downloads/sabnzbd
        - /mnt/bytesized:/downloads/bytesized
        environment:
        - TZ=America/New_York
        - PUID=1000
        - PGID=1000
        network_mode: service:vpn
        depends_on:
        - vpn
        
    pihole:
        container_name: pihole
        hostname: pihole
        restart: always
        image: pihole/pihole:latest
        ports:
        - 53:53
        - 4000:80/tcp
        secrets:
        - pihole-password
        volumes:
        - ./pihole/config:/etc/pihole
        - ./pihole/dnsmasq.d:/etc/dnsmasq.d
        environment:
        - TZ=America/New_York
        - WEBPASSWORD_FILE=/run/secrets/pihole-password

    squid:                                      # used as proxy for vpn server
        container_name: squid
        restart: always
        image: minimum2scp/squid
        network_mode: service:vpn
        depends_on:
        - vpn
