#!/bin/bash
#RClone Config file

RCLONE_CONFIG=/root/.config/rclone/rclone.conf
export RCLONE_CONFIG
LOCKFILE="/var/lock/`basename $0`"
(
  # Wait for lock for 5 seconds
  flock -x -w 5 200 || exit 1

  # Move older local files to the cloud
  /usr/bin/rclone move /home/docker/plex/local encrypt:plex --checkers 3 --fast-list --log-file /home/jglenn/logs/upload.log -v --tpslimit 3 --transfers 3 --delete-empty-src-dirs --min-age 10d

) 200> ${LOCKFILE}

